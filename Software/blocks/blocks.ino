#include <Wire.h>
#include <SPI.h>
#include <Pixy.h>
#include <math.h>
#include <EEPROM.h>
#include <avr/io.h>
#include <avr/interrupt.h>
Pixy pcam;
IntervalTimer myTimer;
#define Buzzer 21
#define Shoot 10
#define PWMlf 38
#define digitalPINlf 39
#define PWMlb  14
#define digitalPINlb  37
#define PWMrf  2
#define digitalPINrf 8
#define PWMrb  7
#define digitalPINrb  6
float Ba, GYa, GBa, reduction = 0;
int BCounter, Bx, By, BxCenter, ByCenter, DistanceB, GYx, GYy, GYxCenter, GYyCenter, DistanceGY, GBx, GBy, GBxCenter, GByCenter, DistanceGB, Gy, GAy,ShootCounter=0,Shootflag=0;
int OS[9] = {0, 0, 0, 0, 0, 0, 0, 0}, OSP[9] = {0, 0, 0, 0, 0, 0, 0, 0}, Compass = 0, Cmp = 0, setcmp = 0, set_s = 0, refresher = 0, Ball = 0, BCount = 0;
char buff[9];
boolean Fr1 = 0, Fr2 = 0, Ri1 = 0, Ri2 = 0, Ba1 = 0, Ba2 = 0, Le1 = 0, Le2 = 0;

//reading Compass

void Read_Cmp()
{
  int cmph, cmpl;
  Wire2.beginTransmission(96);  ////starts communication with cmp03
  Wire2.write(2);                    /////sends the register we wish to read
  Wire2.endTransmission();
  Wire2.requestFrom(96, 2);     /////requests high byte
  Compass = Wire2.read() << 8 | Wire2.read();
  Wire2.endTransmission();
  Compass = map(Compass, 0, 3600, 0, 1023);
  if (Compass - setcmp < 0) Cmp = 1023 - (setcmp - Compass);
  else  Cmp = Compass - setcmp;
  if (Cmp > 512) Cmp = Cmp - 1023;
}
//
////calibration


void Calibration()
{
  byte highByte;
  byte lowByte;

  Serial.println("north");
  delay(5000);

  Wire2.beginTransmission(96);      //starts communication with cmps03
  Wire2.write(15); //Sends the register we wish to read
  Wire2.write(byte(0xFF));
  Wire2.endTransmission();

  Serial.println("east");
  delay(5000);

  Wire2.beginTransmission(96);
  Wire2.write(15);
  Wire2.write(byte(0xFF)); //Sends the register we wish to read
  Wire2.endTransmission();

  Serial.println("south");
  delay(5000);

  Wire2.beginTransmission(96);
  Wire2.write(15);
  Wire2.write(byte(0xFF)); //Sends the register we wish to read
  Wire2.endTransmission();

  Serial.println("west");
  delay(5000);

  Wire2.beginTransmission(96);
  Wire2.write(15);
  Wire2.write(byte(0xFF)); //Sends the register we wish to read
  Wire2.endTransmission();


  while (1) {
    Wire2.beginTransmission(96);
    Wire2.write(1);
    Wire2.endTransmission();

    Wire2.requestFrom(96, 2);        //requests high byte
    highByte = Wire.read();           //reads the byte as an integer
    int bearing = highByte;
    Serial.println(bearing);
    delay(100);
  }
}

//spin_speed
signed int spin_speed(int divided_value, int added_value, int zero_degree)
{
  int compass_input = 0, compass_output = 0;
  compass_input = Cmp ;
  if ( compass_input >= zero_degree )
    compass_output = (compass_input / divided_value) + added_value;
  else if (compass_input <= (-zero_degree) )
    compass_output = -((-compass_input) / divided_value) - added_value;
  else
    compass_output = 0;
  return compass_output;
}
//zard
signed int spin_GY( int x )
{
  int GYa_out;
  if ( GYa > 15) GYa_out = GYa * x;
}

void Counter()
{
  ShootCounter++;
  if (ShootCounter>7) Shootflag=1;
  else Shootflag=0;
  BCounter++;
  if (BCounter > 4) Ball = 0;
  else Ball = 1;
}
void setup()
{
  myTimer.begin(Counter, 150000);
  Serial.begin(9600);
  analogWriteResolution(10);
  pcam.init();
<<<<<<< HEAD
    kaf[6] = analogRead(A6); ///////////L1 
    kaf[7] = analogRead(A7); ///////////L2
    kaf[4] = analogRead(A4); ///////////B1
    kaf[5] = analogRead(A5); ///////////B2
    kaf[0] = analogRead(A0); ///////////F1
    kaf[1] = analogRead(A1); ///////////F2
    kaf[2] = analogRead(A2); ///////////R1
    kaf[3] = analogRead(A3); ///////////R2
    num[0] = kaf[0] + 25;
    num[1] = kaf[1] + 30;
    num[2] = kaf[2] + 30;
    num[3] = kaf[3] + 30;
    num[4] = kaf[4] + 25;
    num[5] = kaf[5] + 25;
    num[6] = kaf[6] + 30;
    num[7] = kaf[7] + 30;
=======
  Wire2.begin();
  ////Motors
  pinMode(39, OUTPUT);
  pinMode(38, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(37, OUTPUT);
  pinMode(7,  OUTPUT);
  pinMode(6,  OUTPUT);
  pinMode(2,  OUTPUT);
  pinMode(8,  OUTPUT);
  analogWriteFrequency(38, 29296.875);
  ///Out Sensors
  pinMode(31, INPUT);//49
  pinMode(32, INPUT);
  pinMode(18, INPUT);
  pinMode(19, INPUT);
  pinMode(22, INPUT);
  pinMode(23, INPUT);
  pinMode(16, INPUT);
  pinMode(17, INPUT);
  //shoot
  pinMode(Shoot, OUTPUT);
  // Battery feedback
  pinMode(20, INPUT);
  pinMode(27, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(24, OUTPUT);
  // set Compass
  //  while (setcmp==0)
  //  {
  //    Read_Cmp();
  //  setcmp = Compass;
  //  }
  /// Buzzer
  pinMode(21, OUTPUT);
  digitalWrite(21, HIGH);
  delay(50);
  digitalWrite(21, LOW);
  delay(30);
  digitalWrite(21, HIGH);
  delay(50);
  digitalWrite(21, LOW);
  pinMode(29, INPUT);
  //    digitalWrite(Shoot,HIGH);
  //    delay(25);
  //    digitalWrite(Shoot,LOW);
  setcmp = (EEPROM.read(0)  << 8) | EEPROM.read(1);
  OSP[1] = (EEPROM.read(2)  << 8) | EEPROM.read(3);
  OSP[2] = (EEPROM.read(4)  << 8) | EEPROM.read(5);
  OSP[3] = (EEPROM.read(6)  << 8) | EEPROM.read(7);
  OSP[4] = (EEPROM.read(8)  << 8) | EEPROM.read(9);
  OSP[5] = (EEPROM.read(10) << 8) | EEPROM.read(11);
  OSP[6] = (EEPROM.read(12) << 8) | EEPROM.read(13);
  OSP[7] = (EEPROM.read(14) << 8) | EEPROM.read(15);
  OSP[8] = (EEPROM.read(16) << 8) | EEPROM.read(17);

>>>>>>> 03503b4468c15ac96c8cf551303964ec1568c469
}

void loop()
{
  //
  reduction = 1;
  //    Calibration();
  OS_Comparison();
  OS_Reader();
  Read_Cmp();
  col_ang();
  set_s = -spin_speed(1, 50, 50);
  if (digitalRead(29) == LOW)
  {
    while (digitalRead(29) == LOW)
    {
      digitalWrite(21, HIGH);
      ///set out sensors on green
      OSP[1] = analogRead(31); //F1 49
      OSP[2] = analogRead(32); //F2
      OSP[3] = analogRead(23); //R1
      OSP[4] = analogRead(22); //R2
      OSP[5] = analogRead(18); //L1
      OSP[6] = analogRead(19); //L2
      OSP[7] = analogRead(16); //B1
      OSP[8] = analogRead(17); //B2
      Read_Cmp();
      setcmp = Compass;
    }
    EEPROM.write(0, highByte(setcmp));  EEPROM.write(1, lowByte(setcmp));
    EEPROM.write(2, highByte(OSP[1]));  EEPROM.write(3, lowByte(OSP[1]));
    EEPROM.write(4, highByte(OSP[2]));  EEPROM.write(5, lowByte(OSP[2]));
    EEPROM.write(6, highByte(OSP[3]));  EEPROM.write(7, lowByte(OSP[3]));
    EEPROM.write(8, highByte(OSP[4]));  EEPROM.write(9, lowByte(OSP[4]));
    EEPROM.write(10, highByte(OSP[5])); EEPROM.write(11, lowByte(OSP[5]));
    EEPROM.write(12, highByte(OSP[6])); EEPROM.write(13, lowByte(OSP[6]));
    EEPROM.write(14, highByte(OSP[7])); EEPROM.write(15, lowByte(OSP[7]));
    EEPROM.write(16, highByte(OSP[8])); EEPROM.write(17, lowByte(OSP[8]));
    digitalWrite(21, LOW);
  }

  /*Serial.print(Ball);Serial.print(" * ");Serial.print(BCount);Serial.print(" * ");Serial.print(BCounter);Serial.print(" * ");Serial.println(Ba);*/
//    Serial.print(BCounter);Serial.print(" * ");Serial.print(Shootflag);Serial.print(" * ");Serial.println(Ball);
//    Serial.print(GYa); Serial.print(" * "); Serial.println(DistanceGY);
//    delay(50);
//  Serial.println(DistanceB);
//  delay(50);
  //mot_ang(90);
  //  test
  //for (int i=0; i<=360 ; i++)
  //{
  //
  //  mot_ang(i);
  //   delay(1);
  //}
  //MOTOR(0,0,0,0);
  //  if(DistanceGY<4)
  //  {
  //    mot_ang(180);
  //  }
  //  else STOP();
  // if (GYa>200)
  //  {
  ////    Serial.println("90");
  ////    delay(10);
  //    mot_ang(90);
  //  }
  //  else if (GYa<150)
  //  {
  ////    Serial.println("270");
  ////    delay(10);
  //    mot_ang(270);
  //  }
  //     Show out sensors
//    sprintf(buff,"F1 %d F2 %d R1 %d R2 %d B1 %d B2 %d L1 %d L2 %d" , Fr1,Fr2,Ri1,Ri2,Ba1,Ba2,Le1,Le2 /*OS[1],OS[2],OS[3],OS[4],OS[7],OS[8],OS[5],OS[6]*/);
//    Serial.println(buff);
//    Serial.print(analogRead(22));Serial.print(" * ");Serial.println(analogRead(17));
//      delay(50);
  //  Serial.print(GYa);
  //  delay(50);
  //  Serial.println(Ba);
  //  if (GYa>200)
  //  {
  //    digitalWrite(Buzzer, HIGH);
  ////    mot_ang(90);
  //  }
  //  digitalWrite(Buzzer, LOW);
  //    if (BCount - BCounter == 0 )
  //    {
  //      Ball=0;
  //    }
//  MOTOR(1023+set_s,1023+set_s,-1023+set_s,-1023+set_s);
  ////////////////FORWARD
    if (Ball==1)
    {
//      follow();
     fol_out();
    }
    else STOP();
    Do_Shoot();

  //////////////////GOALIE
//  if (Ball == 1)
//  {
//        if (DistanceB < 40)  
//            fol_out();
//    //        follow();
//
//    else MoveWidth();
//  }
//  else if (Ball == 0)
//  {
//    Backtogoal();
////         STOP();
//  }

}
